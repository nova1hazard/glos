<div class="home_slider">
    <div class="home_slider__controls">
        <div class="container">
            <div class="home_slider_arrow__container">
                <div class="home_slider_arrow slick-prev"></div>
                <div class="home_slider_arrow slick-next"></div>
            </div>
        </div>
    </div>
    <div class="slick_slider">
        <div><div class="slick_slider__img" style="background: url('/assets/img/slider/1.jpg')"/></div></div>
        <div><div class="slick_slider__img" style="background: url('/assets/img/slider/2.jpg')"/></div></div>
    </div>
</div>
<script>
    $('.slick_slider').slick({
        infinite: true,
        speed: 300,
        fade: true,
        arrows: true,
        prevArrow: $('.slick-prev'),
        nextArrow: $('.slick-next'),
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 4000,
    });

</script>