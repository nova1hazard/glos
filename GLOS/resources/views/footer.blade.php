<div class="footer_wrapper">
    <div class="container">
        <div class="footer">
            <div class="logo_block footer__logo_block">
                <a href="/">
                    <img class="logo" src="../assets/img/logo.jpg" alt="logo">
                </a>
                <div class="logo_right">
                    <div class="logo_right__title">Название<br>
                        компании</div>
                    <div class="logo_right__subtitle">
                        самая клёвая компания
                    </div>
                </div>
            </div>
            <button type="button" class="btn btn-dark contacts_button footer__contacts_button" data-toggle="modal" data-target="#feedbackModal">Обратная связь</button>
            <div class="contacts_mail">
                © 2012–2013 ЗАО «Компания»  info@name.ru
            </div>
            <div class="contacts_nav footer__contacts_nav">
                <ul>
                    <li>
                        <a href="#">Главная</a>
                    </li>
                    <li>
                        <a href="#">Каталог</a>
                    </li>
                    <li>
                        <a href="#">Доставка и оплата</a>
                    </li>
                    <li>
                        <a href="#">Прайс-лист</a>
                    </li>
                    <li>
                        <a href="#">Контакты</a>
                    </li>
                </ul>
            </div>
            <div class="contacts_right">
                <div class="contacts_right__icon"></div>
                <div class="contacts_right__desc">
                    <a href="#">Разработка сайта</a> —
                    <br>
                    компания «ГЛОС»
                </div>
            </div>
        </div>
    </div>
</div>