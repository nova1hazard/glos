<div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="feedbackModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content fb_modal__wrapper">
            <a class="fb_modal__close" data-dismiss="modal"><svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="15px" height="15px">
                    <path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
                          d="M15.008,14.440 L14.440,15.007 L7.500,8.067 L0.560,15.007 L-0.008,14.440 L6.932,7.500 L-0.008,0.559 L0.560,-0.008 L7.500,6.932 L14.440,-0.008 L15.008,0.559 L8.067,7.500 L15.008,14.440 Z"/>
                </svg></a>
            <div class="fb_modal text-center">
                <div class="fb_modal__title">Обратная связь</div>
                <form class="fb_form needs-validation" novalidate name="test" method="get" action="/#">
                    <div class="fb_form__controll">
                        Ваше имя*<br>
                        <input id="name" class="grey_form_input form-control" required>
                        <div class="invalid-feedback fb_modal__invalid">
                            Введите имя
                        </div>
                    </div>
                    <div class="fb_form__controll">
                        Телефон*<br>
                        <input id="phone" class="grey_form_input form-control" required>
                        <div class="invalid-feedback fb_modal__invalid">
                            Введите телефон
                        </div>
                    </div>
                    <div class="fb_form__controll">
                        Емейл*<br>
                        <input id="email" class="grey_form_input form-control" required>
                        <div class="invalid-feedback fb_modal__invalid">
                            Поле обязательно для заполнения
                        </div>
                    </div>
                    <div class="fb_form__controll">
                        Сообщение<br>
                        <textarea id="message" class="grey_form_input form-control" width="100%" required></textarea>
                        <div class="invalid-feedback fb_modal__invalid">
                            Введите сообщение
                        </div>
                    </div>
                    <button class="btn btn-dark greybutton" type="submit">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>