<div class="modal fade" id="smallNav" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="max-width: calc(100vw - 20px);">
            <div class="modal-header small_nav_mh">
                <a class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><svg
                                xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="23px" height="23px">
<path fill-rule="evenodd"  fill="rgb(0, 0, 0)"
      d="M22.354,21.646 L21.646,22.354 L11.500,12.207 L1.354,22.354 L0.646,21.646 L10.793,11.500 L0.646,1.354 L1.354,0.646 L11.500,10.793 L21.646,0.646 L22.354,1.354 L12.207,11.500 L22.354,21.646 Z"/>
</svg></span>
                </a>
            </div>
            <div class="modal-body">
                <div class="contacts_nav small_nav_menu">
                    <ul>
                        <li>
                            <a href="#">Главная</a>
                        </li>
                        <li>
                            <a href="#">Каталог</a>
                        </li>
                        <li>
                            <a href="#" class="small_nav_active">Доставка и оплата</a>
                        </li>
                        <li>
                            <a href="#">Прайс-лист</a>
                        </li>
                        <li>
                            <a href="#">Контакты</a>
                        </li>
                        @guest
                            <li>
                                <a href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>

                            @if (Route::has('register'))
                            <li>
                                <a href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                            @endif

                        @else
                            <li>
                                <b><a href="" class=""><i class="fas fa-user-tie"></i> {{ Auth::user()->name }}</a></b>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                            </li>
                        @endguest
                    </ul>
                </div>
                <div class="contact_block">
                    <a href="tel:+74997777777">+7(499)777-77-77</a><br>
                    <a href="tel:+74997777777">+7(499)777-77-77</a><br>
                </div>
            </div>
        </div>
    </div>
</div>