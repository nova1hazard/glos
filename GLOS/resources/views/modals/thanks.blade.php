<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="max-width: calc(100vw - 20px);">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Успех</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Спасибо за оставленный отзыв!
            </div>
            <div class="modal-footer">
                <button type="button"data-dismiss="modal"  class="btn btn-primary">ОК</button>
            </div>
        </div>
    </div>
</div>