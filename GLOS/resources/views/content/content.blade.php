<div class="container">
    <div class="bc">
        <ul>
            <li>
                <a href="#">Главная</a>
            </li>
            <li>
                <a href="#">Каталог</a>
            </li>
            <li>
                Бытовая техника
            </li>
        </ul>
    </div>
</div>
<div class="content_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('content.leftblock')
            </div>
            <div class="col-md-9">
                @include('content.centerblock')
            </div>
        </div>
    </div>
</div>