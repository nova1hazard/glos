<div class="center_block">
    <div class="prod_container">
        <div class="row">
            <div class="col-md-4">
                <div class="prod_card">
                    <img class="prod_card__img img-fluid" src="/assets/img/nophoto.png" alt="prod_card">
                    <div class="prod_card__desc">Самый клевый чайник, который надо купить!
                    </div>
                    <div class="prod_card__price_block">
                        <div class="prod_card__price">1 900 р.</div>
                        <div class="prod_card__inbox" data-toggle="modal" data-target="#inStockModal"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="prod_card">
                    <img class="prod_card__img img-fluid" src="/assets/img/nophoto.png" alt="prod_card">
                    <div class="prod_card__desc">Самый клевый чайник, который надо купить!
                    </div>
                    <div class="prod_card__price_block">
                        <div class="prod_card__price">1 900 р.</div>
                        <div class="prod_card__inbox" data-toggle="modal" data-target="#inStockModal"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="prod_card">
                    <img class="prod_card__img img-fluid" src="/assets/img/nophoto.png" alt="prod_card">
                    <div class="prod_card__desc">Самый клевый чайник, который надо купить!
                    </div>
                    <div class="prod_card__price_block">
                        <div class="prod_card__price">1 900 р.</div>
                        <div class="prod_card__inbox" data-toggle="modal" data-target="#inStockModal"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="prod_card">
                    <img class="prod_card__img img-fluid" src="/assets/img/nophoto.png" alt="prod_card">
                    <div class="prod_card__desc">Самый клевый чайник, который надо купить!
                    </div>
                    <div class="prod_card__price_block">
                        <div class="prod_card__price">1 900 р.</div>
                        <div class="prod_card__inbox" data-toggle="modal" data-target="#inStockModal"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pg_wrapper">
        <div class="pagination">
            <ul>
                <li><a href="#"></a>1</li>
                <li><a href="#"></a>2</li>
                <li class="pg_active"><a href="#"></a>3</li>
                <li><a href="#"></a>4</li>
                <li><a href="#"></a>5</li>
                <li><a href="#"></a>6</li>
                <li><a href="#"></a>7</li>
            </ul>
        </div>
    </div>
    <div class="grey_line">
    </div>
    <div class="center_block__bottom_text">
        <div class="row">
            <div class="col-md-6">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu tellus gravida, fringilla lorem ac, aliquet felis. Aliquam auctor, arcu id consequat accumsan, eros ipsum eleifend orci, nec ultrices nulla dui a lectus. Sed vitae metus in lorem sollicitudin lacinia. Praesent iaculis laoreet turpis, eget pulvinar leo consectetur eget. Morbi blandit tempor tellus ut aliquet. Aliquam pretium ex vel risus aliquet, id mollis velit dignissim. Proin ornare lacus ac ligula sodales dictum nec ac enim. Mauris nisi tellus, finibus id diam nec, feugiat maximus orci. Sed at varius felis. t
            </div>
            <div class="col-md-6 mt_35">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu tellus gravida, fringilla lorem ac, aliquet felis. Aliquam auctor, arcu id consequat accumsan, eros ipsum eleifend orci, nec ultrices nulla dui a lectus. Sed vitae metus in lorem sollicitudin lacinia. Praesent iaculis laoreet turpis, eget pulvinar leo consectetur eget. Morbi blandit tempor tellus ut aliquet. Aliquam pretium ex vel risus aliquet, id mollis velit dignissim. Proin ornare lacus ac ligula sodales dictum nec ac enim. Mauris nisi tellus, finibus id diam nec, feugiat maximus orci. Sed at varius felis. t
            </div>
        </div>
    </div>
</div>