@extends('layouts.app')

@section('content')

    @include('slider')

    @include('content.content')

    @include('guestbook')

    @include('footer')

    {{--MODALS--}}

    @include('modals.feedback')

    @include('modals.thanks')

    @include('modals.instock')

    @include('modals.small_nav')

@endsection
