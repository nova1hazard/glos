<div class="header">
    <div class="header_top_wrapper">
        <div class="container">
            <div class="header_top nav_desk">
                <div class="logo_block">
                    <a href="/">
                    <img class="logo" src="../assets/img/logo.jpg" alt="logo">
                    </a>
                    <div class="logo_right">
                        <div class="logo_right__title">Название<br>
                        компании</div>
                        <div class="logo_right__subtitle">
                        самая клёвая компания
                        </div>
                    </div>
                </div>
                <div class="contact_block">
                    <a href="tel:+74997777777">+7(499)777-77-77</a><br>
                    <a href="tel:+74997777777">+7(499)777-77-77</a><br>
                    <button type="button" class="btn btn-dark contacts_button mt-2" data-toggle="modal" data-target="#feedbackModal" >Обратная связь</button>
                </div>
            </div>
            <div class="header_top nav_sm">
                <div class="logo_block">
                    <a href="/">
                        <img class="logo" src="../assets/img/logo.jpg" alt="logo">
                    </a>
                    <div class="logo_right">
                        <div class="logo_right__title">Название<br>
                            компании</div>
                        <div class="logo_right__subtitle">
                            самая клёвая компания
                        </div>
                    </div>
                </div>
                <div class="sm_menu_holder">
                    <a class="sm_menu_item ph" href="tel:+74997777777"><i class="fas fa-phone"></i></a>
                    <a class="sm_menu_item" data-toggle="modal" data-target="#smallNav"><i class="fas fa-bars"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="header_nav_wrapper">
        <div class="container">
            <div class="header_nav">
                <ul>
                    <li>
                        <a href="#">Главная</a>
                    </li>
                    <li>
                        <a href="#">Каталог</a>
                    </li>
                    <li>
                        <a href="#">Доставка и оплата</a>
                    </li>
                    <li>
                        <a href="#">Прайс-лист</a>
                    </li>
                    <li>
                        <a href="#">Контакты</a>
                    </li>
                    @guest
                        <li>
                            <a href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>

                        @if (Route::has('register'))
                            <li>
                                <a href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif

                    @else
                        <li>
                            <b><a class=""><i class="fas fa-user-tie"></i> {{ Auth::user()->name }}</a></b>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </div>
</div>