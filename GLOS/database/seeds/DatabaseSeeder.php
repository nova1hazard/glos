<?php

use Illuminate\Database\Seeder;
use App\Role;
use Illuminate\Support\Facades\DB;
use App\User;
use App\RoleUser;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //basic roles
        DB::table('roles')->insert([
            'role' => 'admin',
        ]);
        DB::table('roles')->insert([
            'role' => 'user',
        ]);
        //admin
        $role_id = DB::table('roles')->where('role','admin')->pluck('id');
        $user_id = DB::table('users')->insertGetId([
            'name'           => 'Admin',
            'email'          => 'admin@admin.com',
            'password'       => bcrypt('password'),
            'remember_token' => Str::random(60),
        ]);
        RoleUser::create(['user_id' => $user_id,
            'role_id' => $role_id[0]]);
    }
}
