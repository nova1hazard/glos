<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('reviews/{user_id}', 'AxiosController@getReviews');

//Route::middleware('auth')->group(function() {
//
//});

Route::post('add_review', 'AxiosController@addReview');
Route::get('delete_review/{id}', 'AxiosController@deleteReview');
Route::post('change_like', 'AxiosController@changeLike');
Route::post('add_comment', 'AxiosController@addComment');
Route::get('delete_comment/{id}', 'AxiosController@deleteComment');

Route::get('user_roles/{user_id}', 'AxiosController@getUserRoles');


