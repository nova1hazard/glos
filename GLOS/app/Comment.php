<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'datetime:d.m.y [h:i]',
        'updated_at' => 'datetime:d.m.y [h:i]',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
