<?php

namespace App\Http\Controllers;

use App\Comment;
use App\ReviewLike;
use App\Role;
use App\User;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AxiosController extends Controller
{
    public function getReviews($user_id)
    {
        $reviews = Review::with('owner.roles', 'comments.owner.roles', 'likes.owner.roles')->get()->toArray();
        return ["reviews" => $reviews];
    }

    public function addReview(Request $request)
    {
        Review::create($request->except('token'));
    }

    public function deleteReview($id)
    {
        Review::destroy($id);
    }

    public function changeLike(Request $request)
    {
        $r = $request->except('token');
        $user = Review::find($r['review_id']);
        $res = $user->likes()->where('review_id', $r['review_id'])->get()->toArray();
        isset($res[0]['id'])?ReviewLike::destroy($res[0]['id']):ReviewLike::create($r);
    }

    public function addComment(Request $request)
    {
        Comment::create($request->except('token'));
    }

    public function deleteComment($id)
    {
        Comment::destroy($id);
    }

    public function getUserRoles($user_id)
    {
        return User::find($user_id)->roles()->get()->toArray();
    }
}
