<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\RoleUser;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::INDEX;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'avatar' => ['mimes:jpeg,bmp,png'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $new_avatar = "";
        if(isset($_FILES['avatar']) && $_FILES['avatar']['name']!=="") {
            $current_path = $_FILES['avatar']['tmp_name'];
            $filename = $_FILES['avatar']['name'];
            //new filename
            $new_fname = bin2hex(random_bytes(5));
            $getMime = explode( '.', $filename );
            $res = end( $getMime );
            $new_fname = $new_fname.".".$res;
            //path
            $new_path = public_path().'/assets/img/avatars/'.$new_fname;
            move_uploaded_file($current_path, $new_path);
            $new_avatar = $new_fname;
        }


        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'avatar' => $new_avatar!==""?$new_avatar:"",
            'password' => Hash::make($data['password']),
        ]);
        $user_id = $user->toArray();
        $user_id = $user_id['id'];
        $role_id = DB::table('roles')->where('role','user')->pluck('id');
        RoleUser::create(['user_id' => $user_id,
                          'role_id' => $role_id[0]]);
        return $user;
    }
}
