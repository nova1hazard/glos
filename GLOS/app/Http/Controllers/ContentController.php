<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ContentController extends Controller
{

    public function index()
    {
        $user = "";
        $session_user = Auth::user();
        if($session_user){
            $user = DB::table('users')
                ->where('users.id',$session_user->id)
                ->join('role_user', 'role_user.user_id', '=', 'users.id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->select('users.id','name','roles.role','avatar')
                ->get();
            $user = $user[0];
        }
        return view('welcome')->with([
            'user' => $user,
        ]);
    }

}
