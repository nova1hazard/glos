<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'datetime:d.m.y [h:i]',
        'updated_at' => 'datetime:d.m.y [h:i]',
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function owner()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function likes()
    {
         return $this->hasMany(ReviewLike::class);
    }
}
